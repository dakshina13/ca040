#include<stdio.h>
struct coord{
int x,y;
};
struct coord inpC()
{
    struct coord a;
    printf("Enter the coordinates \n");
    scanf("%d,%d",&a.x,&a.y);
    return a;
}
int evalQ(struct coord a)
{
    if(a.x>=0&&a.y>=0)
        return 1;
    else if(a.x<=0 && a.y>=0)
        return 2;
    else if(a.x<=0 && a.y<=0)
        return 3;
    else if(a.x>=0 && a.y<=0)
        return 4;
    else 
        return 5;
}
void displayQ(int a)
{
    switch(a){
        case 1:
            printf("It is in the first quadrant");
            break;
        case 2:
            printf("It is in the second quadrant");
            break;
        case 3:
            printf("It is in third  quadrant");
            break;
        case 4:
            printf("It is in the fourth quadrant");
            break;
        case 5:
            printf("It is the origin");
            break;
        
    }
}
int main()
{
    struct coord a;
    int r;
    a=inpC();
    r=evalQ(a);
    displayQ(r);
    return 0;
}
