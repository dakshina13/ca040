#include<stdio.h>
#include<string.h>
void inS(char[]);
void changeS(char[],char[]);
void outS(char[],char[]);
int main()
{
    char s[50],c[50];
    inS(s);
    changeS(s,c);
    outS(s,c);
    return 0;
}    
void inS(char z[])
{
    printf("Enter the string \n");
    gets(z);
}
void changeS(char s[],char c[])
{
    for(int i=0;i<strlen(s);i++)
        if(s[i]>=65&& s[i]<=90)
            c[i]=s[i]+32;
        else if(s[i]>=97&& s[i]<=122)
            c[i]=s[i]-32;
        else
            c[i]=s[i];
}
void outS(char s[],char c[])
{
    printf("The entered string is \n");
    puts(s);
    printf("The changed string is \n");
    puts(c);
}