#include<stdio.h>
void inN(int*,int*);
void swapN(int*,int*);
void outN(int*,int*,int);
int main()
{
    int a,b;
    inN(&a,&b);
    outN(&a,&b,1);
    swapN(&a,&b);
    outN(&a,&b,2);
    return 0;
}
void inN(int *c,int *d)
{
    printf("Enter the numbers seperated by comma\n");
    scanf("%d,%d",c,d);
}
void swapN(int *c,int *d)
{
    int temp;
    temp=*c;
    *c=*d;
    *d=temp;
}
void outN(int *c,int *d,int r)
{
    if(r==1)
        printf("The variable before swaping is a=%d and b=%d\n",*c,*d);
    else if(r==2)
        printf("The variable after swaping is a=%d and b=%d\n",*c,*d);
}