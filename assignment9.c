#include<stdio.h>
int inpNum();
void inpA(int[],int);
void outL(int,int);
int posN(int,int[],int);
int inpEle();
int main()
{
    int n,p,ele;
    n=inpNum();
    int a[n];
    inpA(a,n);
    ele=inpEle();
    p=posN(ele,a,n);
    outL(ele,p);
}
int inpNum()
{
    int x;
    printf("Enter the total number of numbers\n");
    scanf("%d",&x);
    return x;
}
void inpA(int b[],int n)
{
    printf("Enter the number of the array\n");
    for(int i=0;i<n;i++)
    {
        printf("%d.",i+1);
        scanf("%d",&b[i]);
    }
}
int inpEle()
{
    int x;
    printf("Enter the search element\n");
    scanf("%d",&x);
    return x;
}
void  outL(int e,int p)
{
    if(p>=0)
        printf("The number %d is present in the array and it is at position %d\n",e,p);
    else
        printf("The number %d is not present in the array\n",e);
}
int posN(int sl,int a[],int n)
{
    for(int i=0;i<n;i++)
        if(a[i]==sl)
            return i;
    return -1;
}