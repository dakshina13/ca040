#include<stdio.h>
void inpS(int *h,int *g);
void inpM(int m,int n,int a[m][n]);
void transM(int m,int n,int a[m][n],int b[n][m]);
void outM(int m,int n,int a[m][n],int b[n][m]);
int main()
{
	int m,n;
	inpS(&m,&n);
	int a[m][n],b[n][m];
	inpM(m,n,a);
	transM(m,n,a,b);
	outM(m,n,a,b);
	return 0;
}
void inpM(int m,int n,int a[m][n])
{
	printf("Enter the elements of the matrix\n");
	for(int i=0;i<m;i++)
		for(int j=0;j<n;j++)
        {
			printf("a[%d][%d]=",i,j);
            scanf("%d",&a[i][j]);
            
        }
}
void inpS(int *h,int *g)
{
	int m,n;
	printf("Enter the size of the array\n");
	scanf("%d,%d",&m,&n);
	*h=m;
	*g=n;
}
void transM(int m,int n,int a[m][n],int b[n][m])
{
    for(int i=0;i<m;i++)
		for(int j=0;j<n;j++)
		{
			if(i==j)
				b[i][j]=a[i][j];
			else
				b[i][j]=a[j][i];
		}
}
void outM(int m,int n,int a[m][n],int b[n][m])
{
	printf("The original matrix is \n");
	for(int i=0;i<m;i++)
	{
		for(int j=0;j<n;j++)
			printf("%d\t",a[i][j]);
		printf("\n");
	}
	printf("The transposed matrix is \n");
	for(int i=0;i<n;i++)
	{
		for(int j=0;j<m;j++)
			printf("%d\t",a[j][i]);
		printf("\n");
	}
}