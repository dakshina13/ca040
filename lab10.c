#include<stdio.h>
void inpN(int*,int*);
void sumN(int*,int*,int*);
void diffN(int*,int*,int*);
void multiplyN(int*,int*,int*);
void divisionN(int*,int*,float*);
void remainderN(int*,int*,int*);
void outR(int*,int*,int*,int*,int*,float*,int*);
int main()
{
    int a,b,s,d,m,r;
    float q;
    inpN(&a,&b);
    sumN(&a,&b,&s);
    diffN(&a,&b,&d);
    multiplyN(&a,&b,&m);
    divisionN(&a,&b,&q);
    remainderN(&a,&b,&r);
    outR(&a,&b,&s,&d,&m,&q,&r);
    return 0;
}
void inpN(int *p1,int *p2)
{
    printf("Enter the two numbers seperated by comma\n");
    scanf("%d,%d",p1,p2);
}
void sumN(int *p1,int *p2,int *s)
{
    *s=*p1+*p2;
}
void diffN(int *p1,int *p2,int *d)
{
    *d=*p1-*p2;
}
void multiplyN(int *p1,int *p2,int *m)
{
    *m=*p1**p2;
}
void divisionN(int *p1,int *p2,float *q)
{
    *q=(float)(*p1)/(*p2);
}
void remainderN(int *p1,int *p2,int *r)
{
    *r=*p1%*p2;
}
void outR(int *a,int *b,int *s,int *d,int *m,float *q,int *r)
{
    printf("The two numbers are %d and %d\n",*a,*b);
    printf("Addition:%d\n",*s);
    printf("Difference:%d\n",*d);
    printf("Multiplication:%d\n",*m);
    printf("Division:%0.2f\n",*q);
    printf("Remainder:%d\n",*r);
}