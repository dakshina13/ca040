#include<stdio.h>
int inpN(int);
int inpNum();
int cLargest(int[],int);
void outL(int);
int main()
{
    int n,l;
    n=inpNum();
    int a[n];
    for(int i=0;i<n;i++)
        a[i]=inpN(i);
    l=cLargest(a,n);
    outL(l);
}
int inpNum()
{
    int x;
    printf("Enter the total number of numbers\n");
    scanf("%d",&x);
    return x;
}
int inpN(int j)
{
    int x;
    printf("%d.",j+1);
    scanf("%d",&x);
    return x;
}
int cLargest(int a[],int x)
{
    int temp=0;
    for(int i=0;i<x;i++)
        if(temp<a[i])
            temp=a[i];
    return temp;
}
void  outL(int l)
{
    printf("The largest number in the array is %d\n",l);
}