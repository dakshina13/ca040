#include<stdio.h>
struct student
{
	char name[20];
	int rn;
	char sec;
	char dept[20];
	int fees;
	int marks;
};
struct student inD(int);
int compM(struct student,struct student);
void outR(struct student,int,int);
int main()
{
	struct student s1,s2;
	int r;
	s1=inD(1);
	s2=inD(2);
	r=compM(s1,s2);
	outR(s1,1,r);
	outR(s2,2,r);
}
struct student inD(int i)
{
	fflush(stdin);
    struct student a;
	printf("Enter the details of student %d\n",i);
	printf("Enter the name is student\n");
	gets(a.name);
	printf("Enter the roll no\n");
	scanf("%d",&a.rn);
    fflush(stdin);
	printf("Enter the section of student\n");
    fflush(stdin);
	scanf("%c",&a.sec);
	printf("Enter the department\n");
    fflush(stdin);
	gets(a.dept);
	printf("Enter the fees of student\n");
	scanf("%d",&a.fees);
	printf("Enter the Marks\n");
	scanf("%d",&a.marks);
	return a;
}
int compM(struct student a,struct student b)
{
	if(a.marks>b.marks)
		return 1;
	else
		return 2;
}
void outR(struct student a,int n,int r)
{
	printf("The details of %d\n",n);
	printf("The name of the student is ");
	puts(a.name);
	printf("The roll no is %d\n",a.rn);
	printf("The section of student is %c\n",a.sec);
	printf("The department is ");
	puts(a.dept);
	printf("The fees of the student %d\n",a.fees);
	printf("The Marks obtained by student is %d\n",a.marks);
	if(n==r)
		printf("This Student scored the highest marks\n");
    printf("\n");
}