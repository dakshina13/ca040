#include<stdio.h>
int inpH();
int inpM();
int comM(int,int);
void outM(int);
int main()
{
    int h,m,rm;
    h=inpH();
    m=inpM();
    rm=comM(h,m);
    outM(rm);
}
int inpH()
{
    int h;
    printf("Enter the hours\n");
    scanf("%d",&h);
    return h;
}
int inpM()
{
    int m;
    printf("Enter the minutes\n");
    scanf("%d",&m);
    return m;
}
int comM(int h,int m)
{
    int rm;
    rm=(h*60)+m;
    return rm;
}
void outM(int rm)
{
    printf("The number of minutes are %d\n",rm);
}
