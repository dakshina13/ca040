#include<stdio.h>
#include<math.h>
int nN();
int reqNum();
int inpN(int);
int countN(int[],int,int);
void outN(int,int);
int main()
{
    int n,num,c;
    n=nN();
    int a[n];
    for(int i=0;i<n;i++)
        a[i]=inpN(i);
    num=reqNum();
    c=countN(a,num,n);
    outN(num,c);
}
int nN()
{
    int n;
    printf("Enter the number of numbers\n");
    scanf("%d",&n);
    return n;
}
int inpN(int  i)
{
    int a;
    printf("%d.",i+1);
    scanf("%d",&a);
    return a;
}
int reqNum()
{
    int n;
    printf("Enter search number\n");
    scanf("%d",&n);
    return n;
}
int countN(int a[],int r,int n)
{
    int c=0;
    for(int i=0;i<n;i++)
        if(a[i]==r)
            c++;
    return c;
}
void outN(int num,int c)
{
    printf("The number %d occurs %d times in the array\n",num,c);
}