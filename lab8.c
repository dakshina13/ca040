#include<stdio.h>
void inS(char[]);
int stringLen(char[]);
void reverseS(char[],char[],int);
int checkS(int,char[],char[]);
void outR(char[],int);
int main()
{
    char a[50],b[50],strlen,res;
    inS(a);
    strlen=stringLen(a);
    reverseS(a,b,strlen);
    res=checkS(strlen,a,b);
    outR(a,res);
    return 0;
}
int stringLen(char z[])
{
    int l=0;
    for(int i=0;z[i]!='\0';i++)
        l++;
    return l;
}
void inS(char z[10])
{
    printf("Enter the string \n");
    gets(z);
}
void reverseS(char x[],char y[],int l)
{
    for(int i=0;i<l;i++)
        y[i]=x[l-i-1];
}
int checkS(int l,char a[],char b[])
{
    for(int i=0;i<l;i++)
        if(a[i]!=b[i])
            return -1;
    return 1;
}
void outR(char a[],int r)
{
    if(r==1)
        printf("The string %s is a palindrone\n",a);
    else
        printf("The string %s is not a palindrome\n",a);
}