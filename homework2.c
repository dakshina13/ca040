#include<stdio.h>
int inpN(int);
int inpNum();
int cLargest(int[],int);
void outL(int,int);
int posN(int,int[],int);
int main()
{
    int n,sl,p;
    n=inpNum();
    int a[n];
    for(int i=0;i<n;i++)
        a[i]=inpN(i);
    sl=cLargest(a,n);
    p=posN(sl,a,n);
    outL(sl,p);
}
int inpNum()
{
    int x;
    printf("Enter the total number of numbers\n");
    scanf("%d",&x);
    return x;
}
int inpN(int j)
{
    int x;
    printf("%d.",j+1);
    scanf("%d",&x);
    return x;
}
int cLargest(int a[],int x)
{
    int l=0,sl=0;
    for(int i=0;i<x;i++)
        if(l<a[i])
        {
            sl=l;
            l=a[i];
        }
        else if(sl<a[i])
            sl=a[i];
    return sl;
}
void  outL(int sl,int p)
{
    printf("The second largest number in the array is %d and it is at position %d\n",sl,p);
}
int posN(int sl,int a[],int n)
{
    for(int i=0;i<n;i++)
        if(a[i]==sl)
            return i;
    return 0;
}