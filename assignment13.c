#include<stdio.h>
void evalGcd(int*,int*,int*);
void inN(int*,int*);
void outR(int*,int*,int*);
int main()
{
    int a,b,gcd;
    inN(&a,&b);
    evalGcd(&a,&b,&gcd);
    outR(&a,&b,&gcd);
}
void inN(int *c,int *d)
{
    printf("Enter the two numbers seperated by comma\n");
    scanf("%d,%d",c,d);
}
void evalGcd(int *c,int *d,int *g)
{
    int a=*c,b=*d;    
    for(int i=1;i<=a&&i<=b;i++)
        if(a%i==0&&b%i==0)
            *g=i;
}
void outR(int *c,int *d,int *g)
{
    printf("The gcd of the two numbers %d and %d is %d\n",*c,*d,*g);
}