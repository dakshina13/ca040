#include<stdio.h>
#include<math.h>
int nN();
int inpN(int);
int cubeN(int);
int squareN(int);
void outN(int[],int[],int[],int);
int main()
{
    int n;
    n=nN();
    int a[n],c[n],s[n];
    for(int i=0;i<n;i++)
        a[i]=inpN(i);
    for(int i=0;i<n;i++)
        c[i]=cubeN(a[i]);
    for(int i=0;i<n;i++)
        s[i]=squareN(a[i]);
    outN(a,c,s,n);
}
int nN()
{
    int n;
    printf("Enter the number of numbers\n");
    scanf("%d",&n);
    return n;
}
int inpN(int  i)
{
    int a;
    printf("%d.",i+1);
    scanf("%d",&a);
    return a;
}
int cubeN(int  a)
{
    return pow(a,3);
}
int squareN(int a)
{
    return pow(a,2);
}
void outN(int a[],int c[],int s[],int n)
{
    for(int i=0;i<n;i++)
        printf("a[%d]=%d square is %d and cube is %d\n",i,a[i],s[i],c[i]);
}