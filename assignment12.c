#include<stdio.h>
struct complexN
{
    int a,b;
};
struct complexN inN(int);
struct complexN addN(struct complexN,struct complexN);
struct complexN subN(struct complexN,struct complexN);
void outR(struct complexN,struct complexN,struct complexN,struct complexN);
int main()
{
    struct complexN n1,n2,a,s;
    n1=inN(1);
    n2=inN(2);
    a=addN(n1,n2);
    s=subN(n1,n2);
    outR(n1,n2,a,s);
    return 0;
}
struct complexN inN(int i)
{
    struct complexN e;
    printf("Enter the number in a+ib\n");
    scanf("%d+i%d",&e.a,&e.b);
    return e;
}
struct complexN addN(struct complexN x,struct complexN y)
{
    struct complexN z;
    z.a=x.a+y.a;
    z.b=x.b+y.b;
    return z;
}
struct complexN subN(struct complexN x,struct complexN y)
{
    struct complexN z;
    z.a=x.a-y.a;
    z.b=x.b-y.b;
    return z;
}
void outR(struct complexN x,struct complexN y,struct complexN m,struct complexN s)
{
    printf("The sum and diiference of the complex numbers %d+i%d and %d+i%d are %d+i%d and %d+i%d respectively\n",x.a,x.b,
    y.a,y.b,m.a,m.b,s.a,s.b);
}