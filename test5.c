#include<stdio.h>
int inpN(int);
int inpNum();
int cLargest(int[],int);
void outL(int[],int[],int);
int posN(int,int[],int);
int cSmallest(int[],int);
void interchangeN(int[],int,int,int,int);
int main()
{
    int n,l,pl,s,ps;
    n=inpNum();
    int a[n],b[n];
    for(int i=0;i<n;i++)
    {
        a[i]=inpN(i);
        b[i]=a[i];
    }
    l=cLargest(a,n);
    s=cSmallest(a,n);
    pl=posN(l,a,n);
    ps=posN(s,a,n);
    interchangeN(b,l,pl,s,ps);
    outL(a,b,n);
}
int inpNum()
{
    int x;
    printf("Enter the total number of numbers\n");
    scanf("%d",&x);
    return x;
}
int inpN(int j)
{
    int x;
    printf("%d.",j+1);
    scanf("%d",&x);
    return x;
}
int cLargest(int a[],int x)
{
    int l=0;
    for(int i=0;i<x;i++)
        if(l<a[i])
            l=a[i];
    return l;
}
int cSmallest(int a[],int x)
{
    int s=a[0];
    for(int i=0;i<x;i++)
        if(s>a[i])
            s=a[i];
    return s;
}
void  outL(int a[],int b[],int n)
{
    printf("The array before interchange is \n");
    for(int i=1;i<=n;i++)
    {
        printf("%d\t",a[i-1]);
        if(i%5==0)
            printf("\n");
    }
    printf("\nThe array after interchange is \n");
    for(int i=1;i<=n;i++)
    {
        printf("%d\t",b[i-1]);
        if(i%5==0)
            printf("\n");
    }
    printf("\n");
}
int posN(int sl,int a[],int n)
{
    for(int i=0;i<n;i++)
        if(a[i]==sl)
            return i;
    return -1;
}
void interchangeN(int b[],int l,int pl,int s,int ps)
{
    b[pl]=s;
    b[ps]=l;
}