#include<stdio.h>
int inputN();
int evalSum(int);
void display(int);
int main()
{
    int n,sum;
    n=inputN();
    sum=evalSum(n);
    display(sum);
}
int inputN()
{
    int n;
    printf("Enter the number\n");
    scanf("%d",&n);
    return n;
}
int evalSum(int n)
{
    int s=0,m=n;
    while(m!=0)
    {
        s+=m%10;
        m=m/10;
    }
    return s;
}
void display(int s)
{
    printf("The sum of  all digits is %d\n",s);
}