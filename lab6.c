#include<stdio.h>
int inpN(int);
int inpNum();
float fAvg(int[],int);
void outA(int[],float,int);
int main()
{
    int n;
    float avg=0;
    n=inpNum();
    int a[n];
    for(int i=0;i<n;i++)
        a[i]=inpN(i);
    avg=fAvg(a,n);
    outA(a,avg,n);
}
int inpNum()
{
    int x;
    printf("Enter the total number of numbers\n");
    scanf("%d",&x);
    return x;
}
int inpN(int j)
{
    int x;
    printf("%d.",j+1);
    scanf("%d",&x);
    return x;
}
float fAvg(int a[],int n)
{
    int sum=0;
    for(int i=0;i<n;i++)
        sum+=a[i];
    return (float)sum/n;
}
void  outA(int a[],float avg,int n)
{
    printf("The average of the array \n");
    for(int i=1;i<=n;i++)
    {
        printf("%d\t",a[i-1]);
        if(i%5==0)
            printf("\n");
    }
    printf("\nis %0.2f\n",avg);
}