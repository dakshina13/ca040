#include<stdio.h>
void inpA(int[]);
int inSE();
int searchA(int[],int);
void outR(int,int);
int main()
{
    int a[10],s,p;
    inpA(a);
    s=inSE();
    p=searchA(a,s);
    outR(s,p);
    return 0;
}
void inpA(int a[])
{
    printf("Enter the elements of array\n");
    for(int i=0;i<10;i++)
        scanf("%d",&a[i]);
}
int inSE()
{
    int a;
    printf("Enter the search element\n");   
    scanf("%d",&a);
    return a;
}
int searchA(int a[],int s)
{
    int p;
    for(int i=0;i<10;i++)
        if(a[i]==s)
        {
            p=i;
            return p;
        }
    return -1;
}
void outR(int s,int p)
{
    if(p>=0)
        printf("The element %d is present in position %d\n",s,p);
    else
        printf("The element %d is not present in the array\n",s);
}